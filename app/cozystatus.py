"""A status display intended for intranet devices."""

from multiprocessing import Process, Manager
from multiprocessing.pool import ThreadPool
import secrets
import subprocess
import time

from flask import Flask, session, render_template, request


def get_server_status(address: str):
    """Get the status of a server."""
    if not address:
        return None
    status_code = subprocess.run(
        ("ping", "-c", "3", "-i", "0.2", "-w", "1", address),
        capture_output=True,
        check=False
    ).returncode
    return status_code


def server_status_loop(server_statuses_shared: dict, *, servers: dict, interval: float):
    """This worker handles pinging of servers and getting their statuses."""

    while True:
        with ThreadPool(processes=len(servers)) as pool:
            server_statuses = dict(zip(
                servers.keys(),
                pool.map(get_server_status, servers.values())
            ))

        server_statuses_shared["time"] = time.strftime("%-I:%M:%S %p", time.localtime())
        server_statuses_shared["statuses"] = server_statuses
        server_statuses_shared["statuses_noheaders"] = tuple(
            (k, v) for k, v in server_statuses.items() if isinstance(v, int)
        )

        time.sleep(interval)


def has_no_empty_params(rule):
    """https://stackoverflow.com/a/13318415"""
    defaults = rule.defaults if rule.defaults is not None else ()
    arguments = rule.arguments if rule.arguments is not None else ()
    return len(defaults) >= len(arguments)


def create_app(*, config_servers, interval: float = 10):
    """Factory to create Flask application."""
    app = Flask(__name__)
    app.config["sitemap"] = []
    app.config["SECRET_KEY"] = secrets.token_hex(256)

    manager = Manager()
    server_statuses_shared = manager.dict()

    server_status_worker = Process(
        target=server_status_loop,
        args=(server_statuses_shared,),
        kwargs={
            "servers": config_servers,
            "interval": interval
        }
    )
    server_status_worker.start()

    @app.route("/")
    def index():
        return render_template("index.jinja")

    @app.route("/network-status")
    def network_status():
        last_checked = server_statuses_shared.get("time")
        if not last_checked:
            last_checked = "never"
        return render_template(
            "network_status.jinja",
            refresh_timer=10,
            last_checked=last_checked,
            server_statuses=server_statuses_shared.get("statuses", {})
        )

    @app.route("/network-status-ticker")
    def network_status_ticker():
        try:
            count = int(request.args.get("count"))
        except Exception:
            count = 1
        last_checked = server_statuses_shared.get("time")
        if not last_checked:
            last_checked = "never"
        if not isinstance(session.get("network_status_index"), int):
            index = session["network_status_index"] = 0
        else:
            index = session["network_status_index"] + count
        statuses_noheaders = server_statuses_shared.get("statuses_noheaders", tuple())
        if index >= len(statuses_noheaders):
            index = 0
        server_statuses = dict(statuses_noheaders[index:index+count])
        session["network_status_index"] = index
        return render_template(
            "network_status.jinja",
            refresh_timer=5,
            last_checked=last_checked,
            server_statuses=server_statuses
        )

    with app.app_context():
        for rule in app.url_map.iter_rules():
            if "GET" in rule.methods and has_no_empty_params(rule):
                app.config["sitemap"].append(rule.endpoint)
    app.config["sitemap"] = app.config["sitemap"][::-1]

    return app
