"""A network checker written in Flask."""

import json
import os

from cozystatus import create_app


filepath_config_servers = os.path.join("/", "config", "config.json")
with open(filepath_config_servers) as file_object:
    config_servers = json.load(file_object)

app = create_app(config_servers=config_servers)
